<?php 
use Illuminate\Database\Seeder;
class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $states = [
        [ "code"=>"KA", "name"=>"laska"],
        [ "code"=>"LA", "name"=>"labama"],
        [ "code"=>"IH", "name"=>"awaii"],
        [ "code"=>"ZA", "name"=>"rizona"],
        [ "code"=>"AC", "name"=>"alifornia"],
        [ "code"=>"RA", "name"=>"rkansas"],
        [ "code"=>"DI", "name"=>"daho"],
        [ "code"=>"AG", "name"=>"eorgia"],
        [ "code"=>"OC", "name"=>"olorado"],
        [ "code"=>"DE", "name"=>"Delaware"],
        [ "code"=>"CT", "name"=>"Connecticut"],
        [ "code"=>"FL", "name"=>"Florida"],
        [ "code"=>"IL", "name"=>"Illinois"],
        [ "code"=>"IA", "name"=>"Iowa"],
        [ "code"=>"IN", "name"=>"Indiana"],
        [ "code"=>"KS", "name"=>"Kansas"],
        [ "code"=>"KY", "name"=>"Kentucky"],
        [ "code"=>"LA", "name"=>"Louisiana"],
        [ "code"=>"ME", "name"=>"Maine"],
        [ "code"=>"MD", "name"=>"Maryland"],
        [ "code"=>"MA", "name"=>"Massachusetts"],
        [ "code"=>"MI", "name"=>"Michigan"],
        [ "code"=>"MN", "name"=>"Minnesota"],
        [ "code"=>"MS", "name"=>"Mississippi"],
        [ "code"=>"MO", "name"=>"Missouri"],
        [ "code"=>"NE", "name"=>"Nebraska"],
        [ "code"=>"MT", "name"=>"Montana"],
        [ "code"=>"NV", "name"=>"Nevada"],
        [ "code"=>"NH", "name"=>"New"],
        [ "code"=>"NJ", "name"=>"New"],
        [ "code"=>"NM", "name"=>"New"],
        [ "code"=>"NY", "name"=>"New"],
        [ "code"=>"NC", "name"=>"North"],
        [ "code"=>"ND", "name"=>"North"],
        [ "code"=>"OK", "name"=>"Oklahoma"],
        [ "code"=>"OH", "name"=>"Ohio"],
        [ "code"=>"OR", "name"=>"Oregon"],
        [ "code"=>"PA", "name"=>"Pennsylvania"],
        [ "code"=>"RI", "name"=>"Rhode"],
        [ "code"=>"SC", "name"=>"South"],
        [ "code"=>"SD", "name"=>"South"],
        [ "code"=>"TN", "name"=>"Tennessee"],
        [ "code"=>"TX", "name"=>"Texas"],
        [ "code"=>"UT", "name"=>"Utah"],
        [ "code"=>"VT", "name"=>"Vermont"],
        [ "code"=>"VA", "name"=>"Virginia"],
        [ "code"=>"WA", "name"=>"Washington"],
        [ "code"=>"WV", "name"=>"West"],
        [ "code"=>"WI", "name"=>"Wisconsin"],
        [ "code"=>"WY", "name"=>"Wyoming"],
      ];

       foreach($states as $key => $ac){
          //var_dump($ac);
          App\State::create($ac);
      }   
    }
}

