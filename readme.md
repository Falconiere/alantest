
## Sobre este repositorio
  Este repositorio es un test simple donde tenemos que registrar inscripciones,
  Para eso tenemos un simples formulario para registrar un email y su respoectivo
  estado

### Como hacer funcionar el sistema 
  
  Para hacer funcionar necesario tener instalado el composer(https://getcomposer.org/), php y mysql
  
  Una vez que esté clonado el repositorio, tene que configurar el archivco .env con los datos de la base y

  ejecutar los siguientes comandos en secuencia: 

  composer install

  composer dump-autoload

  php artisan migrate:refresh --seed
  
  php artisan serve

  En la terminal va estar la url del sistema:

  "Laravel development server started: <http://127.0.0.1:8000>"






   

