<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Alan test</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  
    <!-- Styles -->
    <style>
      html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        height: 100vh;
        margin: 0;
      }

      *, *:before, *:after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
          box-sizing: border-box;
      }  

      h3,h2{
        padding: 0;
        margin: 0;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;

      } 

      h3{
        padding-bottom: 5px;
      }
      input{
        display: block;
        width: 100%;
        height: 35px;

      }

      label{
        display: block;
        padding-bottom: 20px;
      }

      form{
        display: block;
        width: 500px;
        margin: auto;;
        padding: 20px;
        border: 1px solid #eee;
      }

      table{
        padding: 20px;
        margin-top: 15px;
        border: 1px solid #eee;
      }
    </style>
</head>
<body>
  
  <form action="#" method="post" id="form">

    <h2>Enviar Inscripción</h2>
    <label>
      <h3>E-mail</h3>
      <input type="email" name="email" required="true">
    </label>    
    <label>
      <h3>Seleccionar el estado</h3>
      <select name="state_id">

      <!-- LIST ALL STATES -->
       <?php $states =  App\State::all(); ?>
        @foreach($states as $state)
          <option value="{{$state->id}}">{{$state->name}}</option>
        @endforeach;
        <!-- /LIST ALL STATES -->

      </select>
    </label>    
    <button>Enviar</button>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>

  <table align="center" width="500">
    <thead >
      <tr>
        <th align="center"><h3>Inscripciones</h3></th>
      </tr>  
    </thead>
  </table>
  <table align="center" width="500">
    
    <thead >
      <tr>
        <th align="left">Email</th>
        <th align="left">Estado</th>
      </tr>  
    </thead>
    <tbody>
      <!-- LIST ALL INSCRIPTIONS -->
       <?php $inscripcions =  new App\Inscription();;?>

        @foreach($inscripcions->fetchAll() as $inscripcion)
          <tr>
            <td>{{$inscripcion->email}}</td>
            <td>{{$inscripcion->state->name}}</td>
          </tr>
        @endforeach;
        <!-- /LIST ALL INSCRIPTIONS -->
      
    </tbody>
  </table>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  <script>
    $().ready(function(){
      $("form").on("submit",function(e){
        e.preventDefault();
        var data = $(this).serialize();

        $.post("/inscriptions",data)
        .then(function(success){
          console.log(success);
          if(success.status){
            location.reload();
          }
        })
        .catch(function(error){
          console.log(error);
        });

      });
    });  
  </script>
</body>
</html>