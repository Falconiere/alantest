<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

trait Crud{

  /**
   *  fetch the list
   *
   *  @param App\Controller $object controller child object
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  private function fetchCrud($object, Request $request, $isComplex = false){
  
    if(empty($request->page) && !$isComplex){
    
      return response()->json([
        'status' => true,
        'data' => $object::fetchAll(),
        'msg' => 'ok'
      ]);
    
    }

    $page = intval($request->page);

    if($isComplex) 
      $collection = $object::fetch($page, $request);
    else
      $collection = $object::fetch($page);

    if(empty($collection))
      return response()->json(['status' => false, 'msg' => 'No hay '.$object.' registrados']);

    return response()->json([
      'status' => true,
      'data' => $collection,
      'msg' => 'ok'
    ]);
  }

  /**
   *  get certain element
   *
   *  @param integer the element id
   *  @param App\Controller $object controller child object
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  private function getCrud($object, $id, Request $request, $isComplex = false){
    
    if($isComplex) 
      $elem = $object::fetchBy(null,$id);
    else
      $elem = $object::find($id);
  
    if(empty($elem))
      return response()->json(['status' => false, 'msg' => 'No existe ' .  $object]);

    return response()->json([
      'status' => true,
      'data' => $elem,
      'msg' => 'ok'
    ]);
  
  }

  /**
   *  store a new element of given object
   *
   *  @param App\Controller $object controller child object
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  private function storeCrud($object, Request $request){
  
    try{

      $objectCreated = $this->createObject($object, $request);


      $elem = $object::create($objectCreated);

    
    }catch(\Illuminate\Database\QueryException $e){
      
        return response()->json([
          'status' => false, 
          'msg' => 'Hubo un error creando el elemento', 
          'error' => $e, 'errorMsg' => $e->getMessage()
        ]);
    }

    return response()->json([
      'status' => true,
      'data' => $elem, 
      'msg' => 'Se ha creado el elemento exitosamente', 

    ]);

  }

  /**
   *  update an element of given object
   *
   *  @param integer the element id
   *  @param App\Controller $object controller child object
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  private function updateCrud($object, $id,  Request $request = null, $array = null){
  
   $elem = $object::find($id);

    if(empty($elem))
      return response()->json(['status' => false, 'msg' => 'error, no existe '. $object ]);

    
    $iteratable = (!is_null($request)) ? $this->createObject($object, $request) : $array;

    foreach ($iteratable as $key => $value){
      
      $elem->$key = $value;   
    }

    $status = $elem->save();
    $msg = ($status) ? 'El elemento ha sido actualizado' : 'error';

    return response()->json(['status' => $status, 'msg' => $msg]);
  }

  /**
   *  remove an element of given object
   *
   *  @param integer the element id
   *  @param App\Controller $object controller child object
   *
   *  @return json 
   */
  private function deleteCrud($object, $id){
  
    $elem = $object::find($id);

    if(empty($elem))
      return response()->json(['status' => false, 'msg' => 'error, no existe ' . $object ]);

    $status = $elem->delete();

    $msg = ($status) ? 'El elemento ha sido borrado' : 'error';

    return response()->json(['status' => $status, 'msg' => $msg]);
  }

/**
 *  create a new store object
 *
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return array
 */
  private function createObject($object, request $request){

   //dd($request->all()); 
   return array_filter($request->all(), function($key) use($object){

      return in_array($key, $object->fillable);
   }, ARRAY_FILTER_USE_KEY);

  }


/**
 *  create a new store array
 *
 *  @param array $array  the array filter
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return array
 */
  private function createFromArray(array $array, request $request){

   //dd($request->all()); 
   return array_filter($request->all(), function($key) use($array){

      return in_array($key, $array);
   }, ARRAY_FILTER_USE_KEY);

  }


/**
 *  build an object to be sync on many to many relationships sync format
 *
 *  @param array $lements
 *
 *  @return array
 */
  private function buildSyncObject(array $elements){

    if(empty($elements))
      return [];

    $created = array();
    foreach($elements as $elem){

      $created[$elem['id']] = ['quantity' => $elem['quantity']];
    }

    return $created;
  }


}
