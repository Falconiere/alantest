<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\Crud;
use App\Subscription;

class StateController extends Controller
{
  use Crud;
 /**
 *  fetch the list of states paginated
 *
 *  @param Illuminate\Http\Request $request the request data
 *
 *  @return json 
 */
  public function fetch(Request $request)
  {
    return $this->fetchcrud(new State(), $request, true);
  }

  /**
   *  get a State of given id
   *
   *  @param integer $id the measureUnit's id
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  public function get($id, Request $request){

   return $this->getCrud(new State(), $id, $request, true); 
  }


/**
 *  create a new State
 *
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return json
 */
  public function store(request $request)
  {
     $this->validate($request, [
      'code' => 'string|required|max:2',
      'name' => 'string|required|max:191',
    ]);

    return $this->storeCrud(new State(), $request);
  }

/**
 *  update the State data
 *
 *  @param integer $id  the measureUnit's id
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return json
 */
  public function update($id, request $request)
  {

    if(empty(State::find($id)))
      return response()->json(['status' => false, 'msg' => 'error, no existe el elemento' ]);
    
    $this->validate($request, [
      'code' => 'string|required|max:2',
      'name' => 'string|required|max:191',
    ]);

    return $this->updateCrud(new State(), $id,  $request);
  }

/**
 *  remove the State
 *
 *  @param integer $id  the contract's
 *
 *  @return json
 */
  public function remove($id)
  {
    return $this->deletecrud(new State(), $id);
  }
}
