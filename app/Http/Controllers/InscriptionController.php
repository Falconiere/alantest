<?php
namespace App;
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\Crud;
use App\Inscription;

class InscriptionController extends Controller
{
  use Crud;
 /**
 *  fetch the list of subscrptions paginated
 *
 *  @param Illuminate\Http\Request $request the request data
 *
 *  @return json 
 */
  public function fetch(Request $request)
  {
    return $this->fetchcrud(new Inscription(), $request, true);
  }

  /**
   *  get a Inscription of given id
   *
   *  @param integer $id the measureUnit's id
   *  @param Illuminate\Http\Request $request the request data
   *
   *  @return json 
   */
  public function get($id, Request $request){

   return $this->getCrud(new Inscription(), $id, $request, true); 
  }


/**
 *  create a new Inscription
 *
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return json
 */
  public function store(request $request)
  {
     $this->validate($request, [
      'email' => 'string|required|max:191',
      'state_id' => 'integer|required|exists:states,id',
    ]);

    return $this->storeCrud(new Inscription(), $request);
  }

/**
 *  update the Inscription data
 *
 *  @param integer $id  the measureUnit's id
 *  @param Illuminate\Http\Request $request  the request data
 *
 *  @return json
 */
  public function update($id, request $request)
  {

    if(empty(Inscription::find($id)))
      return response()->json(['status' => false, 'msg' => 'error, no existe el elemento' ]);
    
    $this->validate($request, [
      'email'=> 'string|required|max:191',
      'state_id'=> 'integer|required|exists:subscrptions,id',
    ]);

    return $this->updateCrud(new Inscription(), $id,  $request);
  }

/**
 *  remove the Inscription
 *
 *  @param integer $id  the contract's
 *
 *  @return json
 */
  public function remove($id)
  {
    return $this->deletecrud(new Inscription(), $id);
  }
}
