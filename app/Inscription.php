<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  public $fillable = [
    'email','state_id'
  ];


  public function fetchAll(){
    return self::select('*')->with('state')->get();
  }
  
  //hasMany Relationships
  
  /**
   * Get the state for the inscription.
   *
   * @return Illuminate\Database\Eloquent\Collection
   */
  public function state()
  {
    return $this->hasOne('App\State',"id","state_id");
  }
}